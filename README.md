# jetunity_frontend

> Nuxt.js project

## Build Setup

``` bash
# install dependencies
$ yarn

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

For detailed explanation on how things work, checkout the [Nuxt.js docs](https://github.com/nuxt/nuxt.js).

## Local settings:
vim config.js

```
module.exports = {
  baseURL: 'https://api.jetunity.com/api/',
  mediaURL: 'https://api.jetunity.com/media',
  locales: ['ru', 'en']
}
```
