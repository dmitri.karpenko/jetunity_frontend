export default {
  booking () { return 'forms/booking/' },
  brokers () { return 'forms/brokers/' },
  partnership () { return 'forms/partnership/' }
}
