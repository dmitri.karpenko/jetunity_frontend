import brokers from './brokers'
import forms from './forms'
import pages from './pages'
import referrals from './referrals'
import seo from './seo'
import services from './services'
import vars from './vars'

export default {
  brokers,
  forms,
  pages,
  referrals,
  seo,
  services,
  vars
}
