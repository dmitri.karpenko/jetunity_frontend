export default {
  baseURL: 'https://api-dev.jet.space/api/',
  mediaURL: 'https://api-dev.jet.space/media',
  metrikaID: 66798508,
  locales: ['en', 'ru'],
  siteID: 1,
  theme: 'jetunity' // 'skytec'
}
