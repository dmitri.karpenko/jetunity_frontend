#!/bin/bash
git checkout .
git pull
yarn
./build.jetunity.sh

OUT=$?

if [[ $OUT -eq 0 ]];then
  pm2 reload jetunity
fi
