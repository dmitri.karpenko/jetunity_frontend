const debounceFn = require('../utils/debounce')

directive.debounce = debounceFn

function directive (el, bind) {
  if (bind.value !== bind.oldValue) {
    el.oninput = directive.debounce(function (e) {
      el.dispatchEvent(createNewEvent('change'))
    }, parseInt(bind.value) || 800)
  }
}

// IE Support
function createNewEvent (eventName) {
  let e = document.createEvent('Event')
  if (typeof (Event) === 'function') {
    e = new Event(eventName)
  }

  e.initEvent(eventName, true, true)
  return e
}

module.exports = directive
