export default {
  aboutInfo: 'Information about the company',
  all: 'All',
  booking: {
    addFlight: 'Add more flight',
    arrival: 'Arrival',
    departure: 'Departure',
    flightDate: 'Flight Date',
    yourData: 'Your data'
  },
  clipboard: {
    copied: 'Link copied'
  },
  cookie: {
    accept: 'Accept and continue'
  },
  error: 'Error',
  errors: {
    aboutCompany: 'Information about the company',
    errorOccured: 'An error has occurred',
    400: 'Invalid request',
    403: 'Forbidden',
    404: 'This page does not exist or has been deleted',
    500: 'Internal server error'
  },
  footer: {
    callUs: 'Call us',
    developedBy: 'Designed by'
  },
  forms: {
    buttons: {
      back: 'Back',
      cancel: 'Cancel',
      close: 'Close',
      copy: 'Copy',
      ok: 'OK',
      send: 'Send',
      share: 'Share'
    },
    errors: {
      email: 'Invalid email',
      required: 'Required'
    },
    fields: {
      fullName: 'Full name',
      lastName: 'Last name',
      name: 'Name',
      phone: 'Phone number'
    }
  },
  goBackToMain: 'Go back to the main page',
  moreDetails: 'More details',
  next: 'Next'
}
