export default {
  aboutInfo: 'Информация о компании',
  all: 'Все',
  booking: {
    addFlight: 'Добавить ещё рейс',
    arrival: 'Куда',
    departure: 'Откуда',
    flightDate: 'Дата вылета',
    yourData: 'Ваши данные'
  },
  clipboard: {
    copied: 'Ссылка скопирована'
  },
  cookie: {
    accept: 'Принять и продолжить'
  },
  error: 'Ошибка',
  errors: {
    aboutCompany: 'Информация о компании',
    errorOccured: 'Произошка ошибка',
    400: 'Неправильный запрос',
    403: 'Доступ запрещен',
    404: 'Такой страницы не существует или она была удалена',
    500: 'Внутренняя ошибка'
  },
  footer: {
    callUs: 'Позвоните нам',
    developedBy: 'Дизайн сайта'
  },
  forms: {
    buttons: {
      back: 'Назад',
      cancel: 'Отмена',
      close: 'Закрыть',
      copy: 'Скопировать',
      ok: 'ОК',
      send: 'Отправить',
      share: 'Поделиться'
    },
    errors: {
      email: 'Неправильный e-mail',
      required: 'Обязательное поле'
    },
    fields: {
      fullName: 'Полное имя',
      lastName: 'Фамилия',
      name: 'Имя',
      phone: 'Телефон'
    }
  },
  goBackToMain: 'Вернуться на главную',
  moreDetails: 'Подробнее',
  next: 'Далее'
}
