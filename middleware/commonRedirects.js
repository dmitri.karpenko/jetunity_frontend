import config from '../config'
import { changeLocale } from '../utils/i18n'

async function commonRedirects ({ app, error, params, redirect, req, route, store }) {
  let url = route.fullPath
  let status = 200
  const indexFilesRegEx = /index\.(html|htm|php)$/

  // // искренне считаем, что ссылки на самом сайте все корректные, и их редиректить не нужно (это ускорит работу)
  // try {
  //   if (process.server) {
  //     const response = await app.$axios.get(API.seo.hasRedirect(), { params: { url } })
  //     if (response.data.to_url !== url) {
  //       url = response.data.to_url
  //       status = response.data.status
  //     }
  //   }
  //   // eslint-disable-next-line no-empty
  // } catch (e) {}

  // index files to /
  if (indexFilesRegEx.test(url)) {
    url = url.replace(indexFilesRegEx, '')
    status = Math.max(status, 301)
  }

  // i18n
  // обратный редирект с /ru на /
  const urlParts = url.split('/')
  const locale = urlParts.length > 1 ? urlParts[1] : ''
  if (locale.length === 2 && locale === config.locales[0]) {
    urlParts.splice(1, 1)
    url = urlParts.join('/')
    status = 301
  } else {
    if (params.locale && params.locale.length === 2) {
      if (config.locales.indexOf(params.locale) >= 0) {
        changeLocale(params.locale, app, store)
      } else {
        error({ statusCode: 404, message: 'LocaleError' })
      }
    } else {
      // проверим, указана ли языковая переменная
      const urlParts = url.split('/')
      const locale = urlParts.length > 1 ? urlParts[1] : ''

      if (!locale || config.locales.indexOf(locale) >= 0) {
        changeLocale(locale, app, store)
      } else if (locale && locale.length === 2) {
        // языковая переменная неправильная, либо не указана\
        error({ statusCode: 404, message: 'LocaleError' })
      }
    }
  }

  // remove trailing slash
  if (url.length > 1 && /\/$/.test(url)) {
    url = url.substr(0, url.length - 1)
    status = Math.max(status, 301)
  }

  if (status > 300) {
    return redirect(status, url || '/')
  }
}

export default commonRedirects
