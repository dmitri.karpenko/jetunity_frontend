export const fullpageMixin = {
  data () {
    return {
      fullpageOptions: {
        licenseKey: '867F5E56-39FC45B4-9AC88137-8C22C730',
        menu: false,
        navigation: false,
        responsiveWidth: this.normalScrolling ? 9999 : 768,
        scrollOverflow: true,
        scrollOverflowOptions: {
          scrollbars: false
        },
        dragAndMove: 'mouseonly',
        keyboardScrolling: false,
        anchors: ['block1', 'block2', 'block3', 'block4', 'block5', 'block6'],
        afterRender: this.afterRender,
        afterResponsive: this.afterResponsive,
        onLeave: this.onLeave,
        afterLoad: this.afterLoad
      }
    }
  },

  methods: {
    onLeave (origin, destination, direction) {
      destination.item.classList.add('is-inview')
      if (direction === 'down') {
        // this.$refs.fullpage.api.setAllowScrolling(false)
        // this.$refs.fullpage.api.moveSectionUp()
      }
    },

    afterLoad (origin, destination, direction) {
      destination.item.classList.add('is-inview')
    },

    afterRender () {
      document.addEventListener('click', e => {
        if (e.target.matches('a[href*="#"]')) {
          e.preventDefault()
          const href = e.target.getAttribute('href')
          setTimeout(() => {
            this.$refs.fullpage.api.moveTo(href.substr(1))
          }, 0)
        }
      })
    },

    afterResponsive (isResponsive) {
      // if (isResponsive) {
      //   setTimeout(() => {
      //     this.$refs.fullpage.api.setResponsive(true)
      //     window.dispatchEvent(new Event('resize'))
      //   }, 10)
      // } else {
      //   setTimeout(() => {
      //     this.$refs.fullpage.api.setResponsive(false)
      //     window.dispatchEvent(new Event('resize'))
      //   }, 10)
      // }
    }
  }
}
