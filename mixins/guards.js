import { setNextParam } from '../utils/next'
import eventHub from '../utils/eventHub'
import { mapState } from 'vuex'
import { url } from '../filters'

export const requiresAuthMixin = {
  beforeRouteEnter (to, from, next) {
    next(vm => {
      let cb = () => {
        if (!vm.$store.getters.isAuthenticated) {
          return next({
            name: 'auth-login',
            query: { next: setNextParam(vm.$route) },
            replace: true
          })
        }
      }
      vm.$store.dispatch('my/account').then(cb, cb)
    })
  },

  beforeDestroy () {
    eventHub.$off('global:rejectAuth', this.rejectAuth)
  },

  computed: {
    ...mapState({
      locale: state => state.locale
    })
  },

  created () {
    eventHub.$on('global:rejectAuth', this.rejectAuth)
  },

  methods: {
    rejectAuth () {
      this.$router.push(url({ name: 'index', params: { locale: this.locale } }))
    }
  }
}

export const anonymousMixin = {
  beforeRouteEnter (to, from, next) {
    next(vm => {
      let cb = () => {
        if (vm.$store.getters.isAuthenticated) {
          return next({ name: 'index' })
        }
      }
      vm.$store.dispatch('my/account').then(cb, cb)
    })
  }
}
