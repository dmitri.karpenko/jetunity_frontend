export const hasPrevPageMixin = {
  data () {
    return {
      hasPrevPage: true
    }
  },

  mounted () {
    if (!window.history || !history.length || history.length <= 1) {
      this.hasPrevPage = false
    }
  }
}
