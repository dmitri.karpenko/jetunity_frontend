import { mapGetters } from 'vuex'
import { setLocalStorageItem } from '../utils/localStorage'

export const layoutMixin = {
  computed: {
    ...mapGetters({
      menus: 'vars/menus',
      settings: 'vars/menus'
    })
  },

  mounted () {
    if (this.$route && this.$route.query && this.$route.query.r) {
      setLocalStorageItem('referral', this.$route.query.r)
    }
  }
}
