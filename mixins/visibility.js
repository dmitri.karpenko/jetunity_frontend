import { mapState } from 'vuex'
import eventHub from '../utils/eventHub'

export const visibilityMixin = {
  computed: {
    ...mapState({
      animated: state => state.animated
    })
  },

  methods: {
    visibilityChanged (isVisible, entry) {
      if (isVisible) {
        entry.target.classList.add('is-inview')
      }
    }
  }
}
