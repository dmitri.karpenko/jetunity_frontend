import eventHub from '../utils/eventHub'
import { Alert } from '../utils/alert'
import config from '../config'

export default function ({ $axios, store }) {
  $axios.onRequest(conf => {
    if (conf.url.indexOf('http') !== 0) {
      conf.url = store.state.locale + '/' + conf.url
    }
    if (typeof (conf.params) === 'undefined') {
      conf.params = {}
    }
    if (!conf.headers) {
      conf.headers = {}
    }

    conf.headers['accept-language'] = config.acceptLanguage || 'ru-RU, ru;q=0.9, en-US;q=0.8'
    // conf.params.site_id = config.siteID
    console.log(
      `Making ${conf.method.toUpperCase()} request to ${conf.baseURL}${conf.url}, ${conf.params ? JSON.stringify(conf.params) : ''}`
    )

    // const token = getToken()
    // if (token) {
    //   conf.headers.Authorization = `Bearer ${token}`
    // }

    return conf
  }, error => {
    console.log(error)
    return Promise.reject(error)
  })

  $axios.onError(error => {
    const status = error.response && error.response.status ? (parseInt(error.response.status) || 0) : 0
    if (status === 403) {
      if (process.browser) {
        return eventHub.$emit('global:rejectAuth')
      }
    } else if (status === 502 || status === 504) {
      return Alert('error.server', null, true)
    } /* else if (status === 401) {
      const originalRequest = error.config

      originalRequest.baseURL = ''
      originalRequest._retry = true

      const token = getToken()

      if (!token) {
        return eventHub.$emit('global:rejectAuth')
      }

      return $axios
        .post(API.auth.tokenRefresh(), { token })
        .then(response => {
          // setToken(response.data.token)

          originalRequest.headers.Authorization = `Bearer ${response.data.token}`
          $axios.setToken(response.data.token, 'Bearer')
          return $axios(originalRequest)
        }).catch(() => {
          return eventHub.$emit('global:rejectAuth')
        })
    } */
  })
}
