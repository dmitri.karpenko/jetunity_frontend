import eventHub from '../utils/eventHub'
import { disableBodyScroll, clearAllBodyScrollLocks } from 'body-scroll-lock'

const options = {
  reserveScrollBarGap: true
}

eventHub.$on('disable-body-scroll', (el) => {
  setTimeout(() => {
    disableBodyScroll(el, options)
  }, 100)
})

eventHub.$on('enable-body-scroll', () => {
  clearAllBodyScrollLocks()
})
