import Vue from 'vue'
import 'fullpage.js/vendors/scrolloverflow'
import Fullpage from 'vue-fullpage.js'

Vue.use(Fullpage)
