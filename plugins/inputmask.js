import Vue from 'vue'

let vMask = null
if (process.browser) {
  /* eslint-disable global-require */
  vMask = require('v-mask').VueMaskDirective
  /* eslint-enable global-require */
}

if (vMask) {
  Vue.directive('mask', vMask)
}
