import * as PhotoSwipe from 'photoswipe'
import * as PhotoSwipeUI from 'photoswipe/dist/photoswipe-ui-default'
import 'photoswipe/dist/photoswipe.css'
import 'photoswipe/dist/default-skin/default-skin.css'
import 'photoswipe/dist/default-skin/preloader.gif'
import 'photoswipe/dist/default-skin/default-skin.png'
import 'photoswipe/dist/default-skin/default-skin.svg'
import eventHub from '../utils/eventHub'

function photoswipeInit (items, index) {
  const pswpElement = document.querySelectorAll('.pswp')[0]

  const options = {
    history: false,
    shareEl: false,
    fullscreenEl: false,
    closeOnScroll: false,
    index: index
  }
  const gallery = new PhotoSwipe(pswpElement, PhotoSwipeUI, items, options)
  gallery.listen('gettingData', (index, item) => {
    if (item.w < 1 || item.h < 1) {
      const img = new Image()
      img.onload = function () {
        item.w = this.width
        item.h = this.height
        gallery.updateSize(true)
      }
      img.src = item.src
    }
  })

  gallery.listen('close', () => {
    eventHub.$emit('enable-body-scroll')
  })

  gallery.init()
  eventHub.$emit('disable-body-scroll', pswpElement)
}

export default photoswipeInit
