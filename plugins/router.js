import { execMetrikaMethod } from '../utils/metrika'
import eventHub from '../utils/eventHub'

export default async ({ app }) => {
  app.router.afterEach((to, from) => {
    eventHub.$emit('global:closeMenu')

    if (process.browser) {
      if (to.name !== from.name) {
        window.scrollTo(0, 0)
      }

      execMetrikaMethod('hit', [to.fullPath])

      if (typeof (window.ga) !== 'undefined') {
        window.ga('set', 'page', to.fullPath)
        window.ga('send', 'pageview')
      }

      if (typeof (window.fbq) !== 'undefined') {
        window.fbq('track', 'PageView')
      }

      window.dataLayer = window.dataLayer || []
      window.dataLayer.push({
        event: 'Pageview',
        url: to.fullPath
      })
    }
  })
}
