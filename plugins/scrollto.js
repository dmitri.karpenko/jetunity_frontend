document.addEventListener('click', (e) => {
  if (e.target && (e.target.classList.contains('scrollto') || e.target.closest('.scrollto'))) {
    const item = e.target.classList.contains('scrollto') ? e.target : e.target.closest('.scrollto')
    const target = item.hash

    if (document.querySelectorAll(target).length) {
      e.preventDefault()
      const scrollElement = window.document.scrollingElement || window.document.body || window.document.documentElement
      const elementOffset = document.querySelector(target).getBoundingClientRect().top
      const scrollPosition = window.scrollY
      const documentTop = scrollElement.clientTop
      const header = document.querySelector('.header-middle')
      const headerHeight = header ? header.offsetHeight : 0
      const scrollOffset = Math.round(elementOffset + scrollPosition - documentTop - headerHeight)

      if (item.getAttribute('href').charAt(0) === '#') {
        e.preventDefault()
        window.scroll({
          top: scrollOffset,
          behavior: 'smooth'
        })
        window.onscroll = e => {
          const currentScrollOffset = window.pageYOffset || document.documentElement.scrollTop
          if (currentScrollOffset === scrollOffset) {
            window.onscroll = null
          }
        }
      }
    }
  }
})
