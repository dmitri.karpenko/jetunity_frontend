import { getLocale } from '../utils/i18n'
import config from '../config'

export const SET_LOCALE = (state, locale) => {
  if (config.locales.indexOf(locale) >= 0) {
    state.locale = locale
  }
}

export const ANIMATE = (state) => {
  state.animated = true
}

export const state = () => ({
  locale: getLocale(),
  animated: false
})

export const mutations = {
  SET_LOCALE,
  ANIMATE
}
