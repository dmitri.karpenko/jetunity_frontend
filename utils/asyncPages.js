export async function fetchPage (app, error, url, params) {
  let result = {
    fetchError: false,
    fetchStatus: 200,
    page: {}
  }

  if (typeof (params) === 'undefined') {
    params = {}
  }

  try {
    const response = await app.$axios.get(url, { params })
    result.fetchStatus = response && response.status ? response.status : 0
    result = Object.assign({}, result, response.data || {})
  } catch (e) {
    console.log('error:', e)
    result.fetchError = true
    result.fetchStatus = e.response && e.response.status ? e.response.status : -1

    if (result.fetchStatus >= 400) {
      error({
        statusCode: e.response.status,
        message: e.response.data.detail || ''
      })
    }
  }

  return result
}
