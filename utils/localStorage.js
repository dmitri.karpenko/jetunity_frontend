export function isLocalStorageActive () {
  return process.browser && typeof (localStorage) !== 'undefined' && localStorage.getItem
}

export function getLocalStorageItem (key, defaultValue) {
  if (isLocalStorageActive() && localStorage.getItem(key) !== null) {
    try {
      return localStorage.getItem(key)
    } catch (e) {
      console.log(e)
      return defaultValue || undefined
    }
  }
  return defaultValue || null
}

export function setLocalStorageItem (key, value) {
  if (isLocalStorageActive() && localStorage.setItem) {
    try {
      localStorage.setItem(key, value)
    } catch (e) {
      console.log(e)
      return false
    }
    return true
  }
  return false
}

export function removeLocalStorageItem (key) {
  if (isLocalStorageActive() && localStorage.removeItem) {
    try {
      localStorage.removeItem(key)
    } catch (e) {
      console.log(e)
      return false
    }
    return true
  }
  return false
}
