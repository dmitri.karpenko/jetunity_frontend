import { getLocalStorageItem } from './localStorage'

export const getReferralCode = () => {
  return getLocalStorageItem('referral') || null
}
